// SPDX-License-Identifier: AGPL-3.0-only

use std::{
    io::{Read, Write},
    process::{exit, Command, Stdio},
};

fn main() {
    let args = std::env::args().skip(1);

    let args = args.collect::<Vec<_>>();
    if args.len() == 0 {
        eprintln!("desterr: no command");
        exit(1);
    }

    let mut proc = Command::new(&args[0])
        .args(&args[1..])
        .stderr(Stdio::piped())
        .stdout(Stdio::inherit())
        .stdin(Stdio::inherit())
        .spawn()
        .unwrap();

    let mut cstderr = proc.stderr.take().unwrap();
    let mut buf = [0u8; 1024];
    let mut pstderr = std::io::stderr();

    while let Ok(r) = cstderr.read(&mut buf) {
        if r == 0 {
            break;
        }
        pstderr.write_fmt(format_args!("\x1b[31m")).unwrap();
        pstderr.write(&buf[..r]).unwrap();
        pstderr.write_fmt(format_args!("\x1b[0m")).unwrap();
    }
}
