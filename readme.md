# desterr

Destinguish stderr from stdout, when its not obvious.

## Usage

`desterr <command>`

## Examples

```
echo 'cat: evil: No such file or directory' > evil

desterr cat normal      # error will be red here
desterr cat evil        # error is white
```
